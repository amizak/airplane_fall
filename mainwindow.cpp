#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
        init_ui();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::set_connections()
{

}

void MainWindow::init_ui()
{
    this->set_plot_vec();
    this->setGraph();
}

void MainWindow::set_plot_vec()
{
    this->plot_vec.append(ui->Plot);
}

void MainWindow::set_axesCB_vecs()
{

    this->Xaxes_vec.append(ui->CB_Axes_x__);
    this->Yaxes_vec.append(ui->CB_Axes_y__);

    LM_Plot_xAxes* LMx = new LM_Plot_xAxes();
    ui->CB_Axes_x__->setModel(LMx);
    ui->CB_Axes_x__->setCurrentIndex(vExport::ev_X_g);
    ui->CB_Axes_y__->setModel(LMx);
    ui->CB_Axes_y__->setCurrentIndex(vExport::ev_Y_g);

    this->set_axesnames();
}

void MainWindow::set_axesnames()
{
    for (int i = 0; i < this->plot_vec.length();i++ ) {
        this->plot_vec[i]->yAxis->setLabel(this->Yaxes_vec[i]->currentText());
        this->plot_vec[i]->xAxis->setLabel(this->Xaxes_vec[i]->currentText());
    }
}

void MainWindow::setGraph()
{
//    ui->Plot->addGraph();             //Настраиваем графопостроитель
    this->gr = new QCPCurve(ui->Plot->xAxis, ui->Plot->yAxis);
    set_axesCB_vecs();
}

void MainWindow::plot_draw_graph(QCustomPlot* curPlot,int y_num,int x_num)
{
    set_axesnames();
    curPlot->setLocale(QLocale(QLocale::Russian, QLocale::Russia));
//    ui->Plot->graph(0)->setAntialiased(false);
    tExport Data;
    Data = *this->ExportData;

    int kol_point = Data.getRowCount();
    int drob = ui->hs_drob->value();
    int razr = kol_point/drob+1;
    if (razr > kol_point){razr -=1;}
    QVdouble x = Data.getColumnwithStep(x_num,drob),
             y = Data.getColumnwithStep(y_num,drob),
             t = Data.getColumnwithStep(0,drob);
    int j = 0;


    int r = rand() % 255+1,
        b = rand() % 255+1,
        g = rand() % 255+1;

//    ui->Plot->addGraph();

//    QCPCurve a(ui->Plot->xAxis,ui->Plot->yAxis);
//    a.setData(x,x,y);

    QVector<QCPCurveData> data_gr(razr);
    for (int i = 0; i < razr; ++i) {
        data_gr[i] = QCPCurveData(t.at(i),x.at(i),y.at(i));
    }

    gr->data()->set(data_gr,true);
//    curPlot->graph(0)->data().data()->clear(); // ????????
//    curPlot->graph(0)->setPen(QPen(QColor(r,g,b,255)));
//    curPlot->graph(0)->setData(x, y);

    curPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    curPlot->axisRect()->setupFullAxesBox();
    curPlot->rescaleAxes();

    curPlot->rescaleAxes(true);
    curPlot->replot();
}

void MainWindow::plot_set_manual_mash(QCustomPlot *curPlot,
                                      QSlider* hs_x_max,
                                      QSlider* hs_x_min,
                                      QSlider* vs_y_max,
                                      QSlider* vs_y_min)
{
    curPlot->rescaleAxes(true);
    hs_x_max->setRange(int(curPlot->xAxis->range().lower) - int(curPlot->xAxis->range().size()*0.1)
                      ,int(curPlot->xAxis->range().upper) + int(curPlot->xAxis->range().size()*0.1));
    hs_x_min->setRange(int(curPlot->xAxis->range().lower) - int(curPlot->xAxis->range().size()*0.1)
                      ,int(curPlot->xAxis->range().upper) + int(curPlot->xAxis->range().size()*0.1));
    vs_y_max->setRange(int(curPlot->yAxis->range().lower) - int(curPlot->yAxis->range().size()*0.1)
                      ,int(curPlot->yAxis->range().upper) + int(curPlot->yAxis->range().size()*0.1));
    vs_y_min->setRange(int(curPlot->yAxis->range().lower) - int(curPlot->yAxis->range().size()*0.1)
                      ,int(curPlot->yAxis->range().upper) + int(curPlot->yAxis->range().size()*0.1));

    hs_x_max->setValue(int(curPlot->xAxis->range().upper));
    hs_x_min->setValue(int(curPlot->xAxis->range().lower));
    vs_y_max->setValue(int(curPlot->yAxis->range().upper));
    vs_y_min->setValue(int(curPlot->yAxis->range().lower));

    hs_x_max->setSingleStep(int(curPlot->xAxis->range().upper/10000));
    hs_x_min->setSingleStep(int(curPlot->xAxis->range().lower/10000));
    vs_y_max->setSingleStep(int(curPlot->yAxis->range().upper/10000));
    vs_y_min->setSingleStep(int(curPlot->yAxis->range().lower/10000));

    curPlot->replot();
}
////____Добавить график на плотер_____________________________________
void MainWindow::plot_add_graph(QCustomPlot *curPlot, QSpinBox* SB)
{
    curPlot->addGraph();             //Настраиваем графопостроитель
    curPlot->replot();
    if (SB != nullptr){
        SB->setMaximum(curPlot->graphCount());
        SB->setMinimum(1);
    }
}

////____функция связывания данных с моделью представлением_____________________________________
void MainWindow::connectDatawithTable(QTableView *Table, TableModelFullparams *model)
{
    Table->setModel(model);
    Table->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    qApp->processEvents();
    Table->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
}
////____Нажатие кнопоки расчёта_____________________________________

void MainWindow::on_PB_Start_clicked()
{
    if (this->ExportData != nullptr){
        this->ExportData->data.clear();
        this->ExportData = nullptr;
        this->ExportDatamodel = nullptr;
    }

    switch (ui->CB_Int_type->currentIndex()) {
    case 0:{
        this->ExportData = new tExport();
        IntegratorDP_E(0,0,this->ExportData);
        ExportDatamodel = new TableModelFullparams(this->ExportData,this);
        this->connectDatawithTable(ui->TV_Data,ExportDatamodel);
        this->plot_draw_graph(ui->Plot,vExport::ev_Y_g,vExport::ev_X_g);
    } break;
    case 1:{
        this->ExportData = new tExport();
        IntegratorDP_E(ui->input_da_dt->value(),ui->input_alpha->value()*toR,this->ExportData);
        ExportDatamodel = new TableModelFullparams(this->ExportData,this);
        this->connectDatawithTable(ui->TV_Data,ExportDatamodel);
        this->plot_draw_graph(ui->Plot,vExport::ev_Y_g,vExport::ev_X_g);
    } break;
    case 2:{
        this->ExportData = new tExport();
        IntegratorDP_E1(ui->input_a2->value()*toR, ui->input_da_dt->value()*toR,ui->input_alpha->value()*toR,this->ExportData);
        ExportDatamodel = new TableModelFullparams(this->ExportData,this);
        this->connectDatawithTable(ui->TV_Data,ExportDatamodel);
        this->plot_draw_graph(ui->Plot,vExport::ev_Y_g,vExport::ev_X_g);
    } break;
    case 3:{
        this->ExportData = new tExport();
        Optima(ui->input_a2->value()*toR, ui->input_da_dt->value()*toR,ui->input_alpha->value()*toR,this->ExportData);
        ExportDatamodel = new TableModelFullparams(this->ExportData,this);
        this->connectDatawithTable(ui->TV_Data,ExportDatamodel);
        this->plot_draw_graph(ui->Plot,vExport::ev_Y_g,vExport::ev_X_g);
    } break;
    case 4:{
        this->ExportData = new tExport();
        Optima3(ui->input_a2->value()*toR, ui->input_da_dt->value()*toR,ui->input_alpha->value()*toR,this->ExportData);
        ExportDatamodel = new TableModelFullparams(this->ExportData,this);
        this->connectDatawithTable(ui->TV_Data,ExportDatamodel);
        this->plot_draw_graph(ui->Plot,vExport::ev_Y_g,vExport::ev_X_g);
    } break;
    case 5:{
        this->ExportData = new tExport();
        Optima1(ui->input_a2->value()*toR, ui->input_da_dt->value()*toR,ui->input_alpha->value()*toR,this->ExportData);
        ExportDatamodel = new TableModelFullparams(this->ExportData,this);
        this->connectDatawithTable(ui->TV_Data,ExportDatamodel);
        this->plot_draw_graph(ui->Plot,vExport::ev_Y_g,vExport::ev_X_g);
    } break;
    case 6:{
        QVVdouble table = Optima_s();
        foreach (QVdouble row, table) {
            QString text = "";
            foreach (double val, row){
                text += QString::number(val) + " | ";
            }
            //text+="\n";
            ui->Optima_s_txt->appendPlainText(text);
        }
    } break;
    }
}

////____Нажатие кнопоки Построения графика_____________________________________
void MainWindow::on_B_plot_graph__clicked()
{
    if (ExportData != nullptr){
        this->plot_draw_graph(ui->Plot,ui->CB_Axes_y__->currentIndex(),ui->CB_Axes_x__->currentIndex());
    }
}






void MainWindow::on_B_Export_to_file_clicked()
{
    QString Filename= ui->LE_Filename->text();
    if (Filename == ""){
        Filename = "Vivod_" + ui->SB_curFile_datanum->text();
    } else {
        Filename += "_" + ui->SB_curFile_datanum->text();
    }

    QString Ftype = ui->CB_File_type->currentText();
    QString razd = ";";

    if(ui->CB_File_type->currentIndex() == 0){
        switch (ui->CB_razd_type->currentIndex()) {
        case 0:{
            razd = ";";
        }break;
        case 1:{
            razd = "\t";
        }break;
        case 2:{
            razd = " ";
        }break;
        case 3:{
            razd = ".";
        }break;
        case 4:{
            razd = " | ";
        }break;
        case 5:{
            razd = " - | - ";
        }break;
        default:{
            razd = ";";
        }
        }
    }
    double step;

    if(ui->cb_Export_step_enabled->isChecked()){
        step = ui->SB_Vivod_step->value();
    } else{
        step = 0.0;
    }

    this->ExportData->export_data_to_File(razd,
                             ui->LE_Dir->text(),
                             Filename,
                             Ftype,
                             step);

}

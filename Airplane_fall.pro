QT       += core gui
QT       += charts
QT       += printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    additional_func.cpp \
    data_form.cpp \
    data_holder.cpp \
    gost4401_81.cpp \
    int.cpp \
    main.cpp \
    mainwindow.cpp \
    mathmodel.cpp \
    qcustomplot.cpp

HEADERS += \
    additional_func.h \
    data_form.h \
    data_holder.h \
    gost4401_81.h \
    int.h \
    mainwindow.h \
    mathmodel.h \
    qcustomplot.h

FORMS += \
    mainwindow.ui

TRANSLATIONS += \
    Airplane_fall_ru_RU.ts


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#ifndef INT_H
#define INT_H

#include <QObject>
#include "additional_func.h"
#include "data_holder.h"
#include "gost4401_81.h"
#include "QApplication"
#include <iostream>
#include "math.h"
#include <cstdlib>
#include <fstream>
#include "mathmodel.h"

using namespace std;

////______Интеграторы в виде функций________________________________________________________
class Int_Data
{
public:
    Int_Data() {}
    double V0       = 0.0,
           Teta_c0  = -90.0*toR,
           w_z0     = 0.0,
           teta0    = Teta_c0,
           x0       = 0.0,
           y0       = 10000.0,
           tk       = 10.0,
           m0       = 900.0,
           J_z      = 672.0 * 1.3558,
           ld       = 7.0,
           Sm       = 3.0,
           dt       = 0.0001,
           t_0      = 0.0;
};

double IntegratorDP_E  (double da_dt, double alpha, tExport* data = nullptr);
double IntegratorDP_E1  (double a2, double a1, double a0, tExport* data = nullptr);
double IntegratorDP_RK4(double da_dt, double alpha, tExport* data = nullptr);
double Optima (double a2, double a1, double a0, tExport* data = nullptr);
double Optima3(double a2, double a1, double a0, tExport *data = nullptr);
double Optima1(double a2, double a1, double a0, tExport *data = nullptr);
QVVdouble Optima_s (double pa2_0 = 0.01, double pa1_0 = 0.0, double pa2_k = -0.01, double pa1_k = 0.1, double step_a2=-0.00001,double step_a1 = 0.001);



#endif // INT_H

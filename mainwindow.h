#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "additional_func.h"
#include "data_holder.h"
#include "int.h"
#include "data_form.h"
#include "QTableView"
#include "QComboBox"
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    void set_connections();
    void init_ui();


    tExport* ExportData = nullptr;
    TableModelFullparams *ExportDatamodel = nullptr;
    void connectDatawithTable(QTableView *Table, TableModelFullparams *model);

    // Собираем вектор плотеров на форме
    QVector <QCustomPlot*>  plot_vec;
    void set_plot_vec();
    QCPCurve *gr = nullptr;

    // Собираем вектор выпадающих списков на форме
    QVector <QComboBox*>    Xaxes_vec;
    QVector <QComboBox*>    Yaxes_vec;
    void set_axesCB_vecs();
    // Установка имен осей плотеров
    void set_axesnames();
    // Устанавливаем параметры основного плотера
    void setGraph();

    void plot_draw_graph(QCustomPlot* curPlot,
                        int y_num,int x_num);
    void plot_set_manual_mash(QCustomPlot* curPlot,
                              QSlider* hs_x_max,
                              QSlider* hs_x_min,
                              QSlider* vs_y_max,
                              QSlider* vs_y_min);
    void plot_add_graph(QCustomPlot* curPlot, QSpinBox* SB = nullptr);


private slots:
    void on_PB_Start_clicked();
    void on_B_plot_graph__clicked();
    void on_B_Export_to_file_clicked();
};
#endif // MAINWINDOW_H

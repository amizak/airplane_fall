#ifndef MATHMODEL_H
#define MATHMODEL_H

#include <iostream>
#include "math.h"
#include "gost4401_81.h"
#include "additional_func.h"

double f_dV_dt(double Xa, double m, double Teta_c);
double f_dTeta_c_dt(double Ya, double m, double V, double Teta_c);
double f_dx_dt(double V, double Teta_c);
double f_dy_dt(double V, double Teta_c);
double f_domega_z_dt(double M_z, double J_z);
double f_dteta(double omega_z, double om_a = 0.0);

double f_dV_dt_n(double P, double Xa, double m, double g, double Teta_c);
double f_dTeta_c_dt_n(double nya, double V, double g, double Teta_c);

double f_alfa(double teta,double Teta_c);
double f_alpha(double a0,double a1,double a2,double t);
double f_alpha_dz2(double nyaalpha, int nya);



double f_Xa_E(double alpha, double Sm,double ro,double V);
double f_Xa_gost(double M,double Sm,double y,double V);
double f_Xa(double Cxa,double Sm,double ro,double V);

double f_Ya_E(double alpha, double Sm,double ro, double V);
double f_Ya_gost(double alpha, double Sm,double y,double V);
double f_Ya(double Cya,double Sm,double ro,double V,double alpha);

double f_M_z_E(double alpha,double Sm,double ro,double y,double V,double l);
double f_M_z(double Cxa, double Cya,double Sm,double ro,double V,double l);

double f_M(double V,double a);

double f_C_xa(double alpha);
double f_C_ya(double alpha);

double f_mz(double alpha);

double f_g_p(double y);

double  /// коэффициенты
        cXa(double& alphaDegrees),
        cYa(double& alphaDegrees),
        mZomegaZ(double& alphaDegrees);

#endif // MATHMODEL_H

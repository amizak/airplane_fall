#include "additional_func.h"
#include "string.h"
#include <stdio.h>
#include <stdlib.h>
#include <QDateTime>
double rad_to_deg(double radian_num){
    return radian_num*180.0/pi;
}
double deg_to_rad(double degree_num){
    return degree_num/180.0*pi;
}



TDateTime::TDateTime(int y, int m, int d, int h,int min,int s){
    year = y;
    month = m;
    day = d;
    hh = h;
    mm = min;
    ss = s;
}

TDateTime::TDateTime(std::string Date,std::string Time){
    setDateTime_str(Date,Time);
}

void TDateTime::setDateTime_str (std::string Date,std::string Time){
    int length;
    int j = 0;
    std::string temp,num;
    length = int(Date.length());
    for (int i = 0; i < length+1; i++){
        if ((Date[i] != razdD) && (i<length)){
            num = num + Date[i];
        } else {
            switch (j) {
            case 0:{
                day = std::atoi(num.c_str());
            }break;
            case 1:{
                month = std::atoi(num.c_str());
            }break;
            case 2:{
                year = std::atoi(num.c_str());
            }break;
            }
            num = "";
            j+=1;
        }

    }
    j = 0;
    length = int(Time.length());
    for (int i = 0; i < length+1; i++){
        if ((Time[i] != razdT) && (i<length)){
            num = num + Time[i];
        } else {
            switch (j) {
            case 0:{
                hh = std::atoi(num.c_str());
            }break;
            case 1:{
                mm = std::atoi(num.c_str());
            }break;
            case 2:{
                ss = std::atoi(num.c_str());
            }break;
            }
            num = "";
            j+=1;
        }

    }

}


double Alpha_Date(TDateTime Date, double Omega){
    double d,S_mean,Alpha;
    d = (Date.hh*60*60+Date.mm*60+Date.ss)/86400+367*Date.year-(7*(Date.year+(Date.month+9) / 12)) / 4 + (275*Date.month) / 9 + Date.day - 730531.5;
//    S_mean = 280.46061837+360.98564736629*d;
    S_mean = 24110.54841+8640184.812866*d/36525+0.093104*d*d/36525/36525-0.0000062*d*d*d/36525/36525/36525;
    S_mean = S_mean/3600*15;
    while(S_mean/2>180){
        S_mean = S_mean-360;
    }
    Alpha = S_mean + Omega*(Date.hh*3600+Date.mm*60+Date.ss)*57.45;
    if (Alpha > 360){
        Alpha -= 360;
    }
    if (Alpha < 0){
        Alpha += 360;
    }
    return Alpha;
}


double Alpha_0(QDateTime DT){
  int UTC,hh,mm,ss,month,year,day,hours,minutes;
  double S_mean,seconds,d;
  QString temp;

  hh = DT.toString("h").toInt();
  mm = DT.toString("m").toInt();
  ss = DT.toString("s").toInt();
  year = DT.toString("yyyy").toInt();
  month= DT.toString("M").toInt();
  day = DT.toString("d").toInt();

  UTC=hh*60*60+mm*60+ss;
  d=UTC/86400.0+367*year-(7*(year+(month+9) / 12)) / 4 + (275*month) / 9 + day - 730531.5;
  S_mean=280.46061837+360.98564736629*d;
  while(S_mean/2.0>180) {
    S_mean=S_mean-360;
  }
  return S_mean;
  //writeln('S_mean[deg] >', S_mean:15:10);
  //seconds=S_mean/15*60*60;
  //hours=0;
  //minutes=0;
  //while(seconds>3600) do begin
  //     seconds=seconds-3600;
  //     hours=hours+1;
  //end;
  //while(seconds>60) do begin
  //     seconds=seconds-60;
  //     minutes=minutes+1;
  //end;
  //writeln('S_mean[hh:mm:ss] > ', hours:2, ':', minutes:2, ':', seconds:8:6);
    }


QString prob(int length) {
    QString String_p = "";
    for (int i = 0; i < length; ++i) {
        String_p += " ";
    }
    return String_p;
}

QVdouble matr_x_vec(QVVdouble A, QVdouble X){
//    A*X=X1

    int i, j;
    QVdouble X1;
    if (X.length() == 3){
        X1 = QVdouble(3).fill(0.0);
    for (i = 0; i < 3; i++) {
        X1[i] = 0;
        for (j = 0; j < 3; j++) {
            X1[i] = X1.at(i) + A.at(i).at(j) * X.at(j);
        }
    }
    }

    if (X.length() == 6){
        X1 = QVdouble(6).fill(0.0);
    for (i = 0; i < 3; i++) {
        X1[i] = 0;
        X1[i + 3] = 0; //чтобы умножить матр 3*3 на вектор 6*1
        for (j = 0; j < 3; j++) {
            X1[i] = X1.at(i) + A.at(i).at(j) * X.at(j);
            X1[i + 3] = X1.at(i + 3) + A.at(i).at(j) * X.at(j + 3);
        }
    }

    }
    return X1;


}

QVVdouble matr_nxm_T(QVVdouble A)
{

    QVVdouble AT = A;
    int x = A.length(), y = A.at(0).length();

    for (int i = 0; i < x; i++) {
        for (int j = 0; j < y; j++) {
            AT[i][j] = A.at(j).at(i);
        }
    }
    return AT;

}

#ifndef DATA_HOLDER_H
#define DATA_HOLDER_H
#include "QDateTime"
#include "additional_func.h"
#include "QFile"
#include "QObject"
#include "QApplication"


//------Набор важных и выводимых параметров----------------------------------------

class vExport
{
public:
    vExport();
    QVdouble data = QVector<double>(ev_LAST).fill(0.0);

    enum Export_vec {
        ev_Time_s = 0,
        ev_X_g      ,
        ev_Y_g      ,
        ev_r        ,
        ev_Vx_g     ,
        ev_Vy_g     ,
        ev_V        ,
        ev_om_z     ,
        ev_teta__r  ,
        ev_teta__d  ,
        ev_Teta_c_r ,
        ev_Teta_c_d ,
        ev_alpha_r  ,
        ev_alpha_d  ,
        ev_Density  ,
        ev_Xa       ,
        ev_Ya       ,
        ev_Mz       ,
        ev_LAST
    };
    ~vExport() {
        data.clear();
    }
};

class ED_Settings
{
public:
    ED_Settings(){set_preset_settings(i_All);};
    ED_Settings(int init_type);

    enum init_BD_settings{
        i_All = 0,
        i_Custom,
        i_LAST
    };

    QStringList Settings_preset_name_List = {"Полное",
                                             "Своё"};


    QStringList Settings_name_List = {"Видимость",
                                      "Порядок вывода",
                                      "Формат записи",
                                      "Кол-во знаков",
                                      "Измерение",
                                      "Порядок измерения"};

    enum Setting_num{
        s_Num_Visibility = 0,
        s_Num_showOrder,
        s_Num_format,
        s_Num_floatOrder,
        s_Num_dimType,
        s_Num_dimOrder,
        s_LAST
    };

    void set_preset_settings(int type);

    QVector <bool> visible = QVector<bool> (vExport::ev_LAST).fill(true); //Показывать ли параметр
    QVector <int>  shOrder = QVector<int>  (vExport::ev_LAST).fill(0);     //Порядок взаимного позиционировани-вывода
    QVector <QChar> format = QVector<QChar>(vExport::ev_LAST).fill('f');  //Формат его отображения
    QVector <int>  kolzn   = QVector<int>  (vExport::ev_LAST).fill(6);    //Количество знаков после запятой
    QVector <int>  dimType = QVector<int>  (vExport::ev_LAST).fill(0);     //Тип измерения (кг, м, с, ...)
    QVector <int>  dimOrder= QVector<int>  (vExport::ev_LAST).fill(0);     //Порядок измерение (мм, м, км ...)

};


class tExport{

public:

    explicit tExport();
    explicit tExport(QVVdouble data){
        this->data = data;
    };
    explicit tExport(const tExport & table){
        this->Number    = table.Number;
        this->SLname    = table.SLname;
        this->Settings  = table.Settings;
        this->data      = table.data;
    }
    ~tExport(){}

    tExport& operator= (const tExport &table)
    {
        // Проверка на самоприсваивание
        if (this == &table)
            return *this;

        // Выполняем копирование значений
        this->Number    = table.Number;
        this->SLname    = table.SLname;
        this->Settings  = table.Settings;
        this->data      = table.data;

        // Возвращаем текущий объект
        return *this;
    }

    QVVdouble data;

    QStringList SLname = {
        "Time,  [s]"      ,
        "X_g,   [m]"      ,
        "Y_g,   [m]"      ,
        "r,     [m]"      ,
        "Vx_g,  [m/s]"    ,
        "Vy_g,  [m/s]"    ,
        "V,     [m/s]"    ,
        "om_z,  [1/s]"    ,
        "tetha, [rad]"    ,
        "tetha, [deg]"    ,
        "Tetha, [rad]"    ,
        "Tetha, [deg]"    ,
        "alpha, [rad]"    ,
        "alpha, [deg]"    ,
        "Dens,  [kg/m^3]" ,
        "Xa,    [N]"      ,
        "Ya,    [N]"      ,
        "Mz,    [N]"
    };

    void set_epv(vExport list){this->data.append(list.data);}
    QVVdouble getTable(){return data;}

    void export_data_to_File(QString razd,
                             QString Path,
                             QString FileName = "Vivod",
                             QString Filetype = ".txt",
                             double every_Time= 1);

    QVdouble getRow(int Rnum) const {return data.at(Rnum);}
    int      getRowCount() const {return data.length();}
    QVdouble getColumn(int Cnum) const{
        QVdouble column;
        foreach (QVdouble row, data) {
            column.append(row.at(Cnum));
        }
        return column;
    }

    QVdouble getColumnwithStep(int Cnum, int Step) const{
        QVdouble column;
        int Counter = 0;
        for (int i = 0; i < getRowCount(); ++i) {
            if (Counter <= i || i == getRowCount()-1){
                column.append(data.at(i).at(Cnum));
                qApp->processEvents();
            }
            Counter += Step;
        }
        return column;
    }

    int      getColumnCount() const {return vExport::ev_LAST;}

    double   getVal (int Cnum,int Rnum) const {return data.at(Rnum).at(Cnum);}

    int Number = 0;
    ED_Settings Settings = ED_Settings(ED_Settings::i_All);
} ;






//_________Расчётные параметры______________________________________________

//------Набор основных параметров ЛА----------------------------------------


//------ЛА------------------------------------------------------------------




#endif // DATA_HOLDER_H

#ifndef ADDITIONAL_FUNC_H
#define ADDITIONAL_FUNC_H
#include "time.h"
#include "qvector.h"
#include <string>
#include <iostream>
#include "math.h"
#include "QDateTime"
const double e_         = M_E;//3.1415926535897932384626433832795028841971693993751;
const double pi         = M_PI;//3.1415926535897932384626433832795028841971693993751;
const double pi_2       = M_PI_2;//3.1415926535897932384626433832795028841971693993751;
const double pi_4       = M_PI_4;//3.1415926535897932384626433832795028841971693993751;
const double R_Earth_s  = 6371;
const double R_Earth_e  = 6378.137;
const double mu_Earth   = 398600.44;
const double om_Earth   = 0.000072921158553;
const double Eps_J2     = 2.634*pow(10,10);
const double very_small = pow(10,-6);
const double g          = 9.80665;
const double toD        = 180.0/pi;
const double toR        = pi/180.0;

typedef QVector<double> QVdouble;       //Double massiv
typedef QVector<QVdouble> QVVdouble;    //Double Matrix


double rad_to_deg(double radian_num);
double deg_to_rad(double degree_num);

QVdouble matr_x_vec(QVVdouble A, QVdouble X );

QVVdouble matr_nxm_T(QVVdouble A);

class vXYZ
{
public:
    vXYZ() {}
    vXYZ(double x0,double y0,double z0, bool norma = false) {
        x=x0;   y=y0;   z=z0;
        if (norma){normalize();}
    }
    vXYZ(double fill_num) {
        x=fill_num;   y=fill_num;   z=fill_num;
    }
    vXYZ(QVdouble vec) {set_QVdXYZ(vec);}
    double x = 0.0,y = 0.0,z = 0.0;
    bool norm = false;
    double l = 0.0;
    enum coordinates{
        c_x = 0,
        c_y,
        c_z,
        LAST_coord
    };

    double len (){
        if (norm){
            return l;
        }else {
            return sqrt(x*x+y*y+z*z);
        }
    }

    void normalize (){
        l = len();
        if (l != 0.0){
            x /= len();
            y /= len();
            z /= len();
            norm = true;
        }
    }

    QVdouble get_QVdXYZ(){
        QVdouble vec = QVdouble(LAST_coord).fill(0.0);
        vec[c_x] = x;
        vec[c_y] = y;
        vec[c_z] = z;
        return vec;
    }

    void set_QVdXYZ(QVdouble vec){
        x = vec.at(c_x);
        y = vec.at(c_y);
        z = vec.at(c_z);
        norm = false;
    }

    int kol_params = 3;
};

class vRG
{
    bool norm = false;
public:
    vRG() {}
    vRG(double r0,double l0,double m0,double n0) {
        r=r0;   l=l0;   m=m0;   n=n0;
        norm = false;
        normalize();
    }
    vRG(QVdouble vec) {set_QVdRG(vec);}
    double r = 0.0,
           l = 0.0,
           m = 0.0,
           n = 0.0;
    double length = 0.0;
    enum coordinates{
        rg_r = 0,
        rg_l,
        rg_m,
        rg_n,
        LAST_coord
    };

    double len (){
        if (norm){
            return length;
        }else {
            return sqrt(r*r+l*l+m*m+n*n);
        }
    }

    void normalize (){
        length = len();
        if (length != 0.0){
            r /= len();
            l /= len();
            m /= len();
            n /= len();
            norm = true;
        }
    }

    QVdouble get_QVdRG(){
        QVdouble vec = QVdouble(LAST_coord).fill(0.0);
        vec[rg_r] = r;
        vec[rg_l] = l;
        vec[rg_m] = m;
        vec[rg_n] = n;
        return vec;
    }

    void set_QVdRG(QVdouble vec){
        r = vec.at(rg_r);
        l = vec.at(rg_l);
        m = vec.at(rg_m);
        n = vec.at(rg_n);
        norm = false;
        normalize();
    }

    int kol_params = 4;
};

class TDateTime
{
public:
    int year,month,day;
    int hh,mm,ss;
    std::string sD,sT,sDT;
    char razdD = '.';
    char razdT = ':';

    TDateTime(int y, int m, int d, int h,int min,int s);
    TDateTime(std::string Date,std::string Time);
    TDateTime(){}

    void setDateTime_str (std::string Date,std::string Time);

    ~TDateTime(){
        std::cout << "Destruct DT v" << std::endl;


    }
};

QString prob (int length);
double Alpha_Date(TDateTime Date, double Omega);
double Alpha_0(QDateTime DT);
#endif // ADDITIONAL_FUNC_H

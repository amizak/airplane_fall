#include "int.h"


double IntegratorDP_E(double da_dt, double alpha, tExport* data)
{

    gost_4401_21 atm;

    double V0 = 0.0,
           Teta_c0 = -90.0*toR,
           w_z0 = 0.0,
           teta0 = Teta_c0,
           x0 = 0.0,
           y0 = 10000.0,
           tk = 10.0,
           m0 = 900.0,
           J_z = 672.0 * 1.3558,
           ld = 7.0,
           Sm = 3.0,
           dt = 0.0001,
           t_0 = 0.0;

    int n; double Shag;
    double dV_dt,dTeta_c0,dw_z0,dteta0,dx0,dy0;
    double w_a = da_dt;


    Shag = 0;
    n = round((tk-t_0)/dt)+1;

    bool EoR = false;
    bool EFP = true;

    vExport Step_data;
    while (!EoR){
        //Teta_c0 = Teta_c0*180/ PI; teta0  =  teta0*180/ PI;
        dV_dt = dt*f_dV_dt(f_Xa_E(f_alfa(teta0,Teta_c0),
                                  Sm,
                                  atm.ro_gost(y0),
                                  V0),
                           m0,
                           Teta_c0);
        dTeta_c0 = dt*f_dTeta_c_dt(f_Ya_E(f_alfa(teta0,Teta_c0),
                                          Sm,
                                          atm.ro_gost(y0),
                                          V0),
                                   m0,
                                   V0,
                                   Teta_c0);
        dx0 = dt*f_dx_dt(V0,Teta_c0);
        dy0 = dt*f_dy_dt(V0,Teta_c0);
        dw_z0 = dt*(f_domega_z_dt(f_M_z_E(f_alfa(teta0,Teta_c0),
                                         Sm,
                                         atm.ro_gost(y0),
                                         y0,
                                         V0,
                                         ld),
                                      J_z));
        dteta0 = dt*f_dteta(w_z0,w_a);

        V0  = V0 + dV_dt;
        Teta_c0 = Teta_c0 + dTeta_c0;
        x0 = x0 + dx0;
        y0 = y0 + dy0;
        w_z0 = w_z0 + dw_z0;
        teta0 = teta0 + dteta0;
        t_0  = t_0 +  dt;
        Shag = Shag + 0.1;

        if ((f_alfa(teta0,Teta_c0) > alpha) && EFP){
            w_a = -da_dt;
        } else {
            w_a =  da_dt;
        }

        if (data != nullptr){
            Step_data.data[vExport::ev_Time_s  ] = t_0;
            Step_data.data[vExport::ev_X_g     ] = x0;
            Step_data.data[vExport::ev_Y_g     ] = y0;
            Step_data.data[vExport::ev_r       ] = sqrt(x0*x0+y0*y0);
            Step_data.data[vExport::ev_Vx_g    ] = V0*cos(Teta_c0);
            Step_data.data[vExport::ev_Vy_g    ] = V0*sin(Teta_c0);
            Step_data.data[vExport::ev_V       ] = V0;
            Step_data.data[vExport::ev_om_z    ] = w_z0;
            Step_data.data[vExport::ev_teta__r ] = teta0;
            Step_data.data[vExport::ev_teta__d ] = teta0*toD;
            Step_data.data[vExport::ev_Teta_c_r] = Teta_c0;
            Step_data.data[vExport::ev_Teta_c_d] = Teta_c0*toD;
            Step_data.data[vExport::ev_alpha_r ] = f_alfa(teta0,Teta_c0);
            Step_data.data[vExport::ev_alpha_d ] = f_alfa(teta0,Teta_c0)*toD;
            Step_data.data[vExport::ev_Density ] = atm.ro_gost(y0);
            Step_data.data[vExport::ev_Xa      ] = f_Xa_E(f_alfa(teta0,Teta_c0),
                                                          Sm,
                                                          atm.ro_gost(y0),
                                                          V0);
            Step_data.data[vExport::ev_Ya      ] = f_Ya_E(f_alfa(teta0,Teta_c0),
                                                          Sm,
                                                          atm.ro_gost(y0),
                                                          V0);
            Step_data.data[vExport::ev_Mz      ] = f_M_z_E(f_alfa(teta0,Teta_c0),
                                                           Sm,
                                                           atm.ro_gost(y0),
                                                           y0,
                                                           V0,
                                                           ld);
            data->set_epv(Step_data);
        }

        if (y0 < 0.0){
            EoR = true;
        }
        if (t_0 > 300.0){
            EoR = true;
        }
       }

    return x0;
}

double IntegratorDP_E1(double a2, double a1, double a0, tExport* data)
{

    gost_4401_21 atm;

    double V0 = 0.0,
           Teta_c0 = -90.0*toR,
           w_z0 = 0.0,
           teta0 = Teta_c0,
           x0 = 0.0,
           y0 = 10000.0,
           tk = 10.0,
           m0 = 900.0,
           J_z = 672.0 * 1.3558,
           ld = 7.0,
           Sm = 3.0,
           dt = 0.01,
           t_0 = 0.0;

    int n; double Shag;
    double dV_dt,dTeta_c0,dw_z0,dteta0,dx0,dy0;
  //  double w_a = da_dt;


    Shag = 0;
    n = round((tk-t_0)/dt)+1;

    bool EoR = false;
    bool EFP = true;

    vExport Step_data;
    while (!EoR){
        //Teta_c0 = Teta_c0*180/ PI; teta0  =  teta0*180/ PI;
        dV_dt = dt*f_dV_dt(f_Xa_E(f_alpha(a0,a1,a2,t_0),
                                  Sm,
                                  atm.ro_gost(y0),
                                  V0),
                           m0,
                           Teta_c0);
        dTeta_c0 = dt*f_dTeta_c_dt(f_Ya_E(f_alpha(a0,a1,a2,t_0),
                                          Sm,
                                          atm.ro_gost(y0),
                                          V0),
                                   m0,
                                   V0,
                                   Teta_c0);
        dx0 = dt*f_dx_dt(V0,Teta_c0);
        dy0 = dt*f_dy_dt(V0,Teta_c0);
       /* dw_z0 = dt*(f_domega_z_dt(f_M_z_E(f_alfa(teta0,Teta_c0),
                                         Sm,
                                         atm.ro_gost(y0),
                                         y0,
                                         V0,
                                         ld),
                                      J_z));
        dteta0 = dt*f_dteta(w_z0,w_a);*/

        V0  = V0 + dV_dt;
        Teta_c0 = Teta_c0 + dTeta_c0;
        x0 = x0 + dx0;
        y0 = y0 + dy0;
      //  w_z0 = w_z0 + dw_z0;
      //  teta0 = teta0 + dteta0;
        teta0=Teta_c0+f_alpha(a0,a1,a2,t_0);
        t_0  = t_0 +  dt;
        Shag = Shag + 0.1;


        if (data != nullptr){
            Step_data.data[vExport::ev_Time_s  ] = t_0;
            Step_data.data[vExport::ev_X_g     ] = x0;
            Step_data.data[vExport::ev_Y_g     ] = y0;
            Step_data.data[vExport::ev_r       ] = sqrt(x0*x0+y0*y0);
            Step_data.data[vExport::ev_Vx_g    ] = V0*cos(Teta_c0);
            Step_data.data[vExport::ev_Vy_g    ] = V0*sin(Teta_c0);
            Step_data.data[vExport::ev_V       ] = V0;
            Step_data.data[vExport::ev_om_z    ] = w_z0;
            Step_data.data[vExport::ev_teta__r ] = teta0;
            Step_data.data[vExport::ev_teta__d ] = teta0*toD;
            Step_data.data[vExport::ev_Teta_c_r] = Teta_c0;
            Step_data.data[vExport::ev_Teta_c_d] = Teta_c0*toD;
            Step_data.data[vExport::ev_alpha_r ] = f_alpha(a0,a1,a2,t_0);
            Step_data.data[vExport::ev_alpha_d ] = f_alpha(a0,a1,a2,t_0)*toD;
            Step_data.data[vExport::ev_Density ] = atm.ro_gost(y0);
            Step_data.data[vExport::ev_Xa      ] = f_Xa_E(f_alpha(a0,a1,a2,t_0),
                                                          Sm,
                                                          atm.ro_gost(y0),
                                                          V0);
            Step_data.data[vExport::ev_Ya      ] = f_Ya_E(f_alpha(a0,a1,a2,t_0),
                                                          Sm,
                                                          atm.ro_gost(y0),
                                                          V0);
            Step_data.data[vExport::ev_Mz      ] = f_M_z_E(f_alpha(a0,a1,a2,t_0),
                                                           Sm,
                                                           atm.ro_gost(y0),
                                                           y0,
                                                           V0,
                                                           ld);
            data->set_epv(Step_data);
        }

        if (y0 < 0.0){
            EoR = true;
        }
        if (t_0 > 300.0){
            EoR = true;
        }
       }

    return x0;
}


double IntegratorDP_RK4(double da_dt, double alpha, tExport* data)
{

//    int n;
////53 54 54 84	0 0 0 3 0 9.81 1.225 101325 120 0.2 288.15 0.14 0.2289 0.1 1180

//    double V0 = 0.0,
//           Teta_c0 = -90.0*toR,
//           w_z0 = 0.0,
//           teta0 = 0.0,
//           x0 = 0.0,
//           y0 = 3000.0,
//           tk = 10.0,
//           m0 = 900.0,
//           J_z = 672.0 * 1.3558,
//           ld = 7.0,
//           Sm = 3.0,
//           dt = 0.1,
//           t_0 = 0.0;
//    double Shag;
//    n = round((tk-t_0)/dt)+1;
//    Teta_c0 = Teta_c0* pi/180; teta0 = teta0* pi/180;

//    double V1,Teta_c1,w_z1,teta1,x1,y1,KdV1,KdTeta_c1,Kdx1,Kdy1,Kdw_z1,Kdteta1,
//    V2,Teta_c2,w_z2,teta2,x2,y2,KdV2,KdTeta_c2,Kdx2,Kdy2,Kdw_z2,Kdteta2,
//    V3,Teta_c3,w_z3,teta3,x3,y3,KdV3,KdTeta_c3,Kdx3,Kdy3,Kdw_z3,Kdteta3,
//    KdV4,KdTeta_c4,Kdx4,Kdy4,Kdw_z4,Kdteta4,t2,t1,t3;



//    for (int i = 1; i < n+1; i++)  {
//     //K1____________________________________________________________________________
//        KdV1       =   dt*f_dV_dt(f_Xa_E(f_M(V0,f_a_E(T_ON,y0)),Sm,ro_ON,y0,V0),
//                                  g,
//                                  Teta_c0);
//        KdTeta_c1  =   dt*f_dTeta_c_dt(f_P_E(f_P0(msek),Sa,p_ON,y0),
//                                   f_alfa(teta0,Teta_c0),
//                                   f_Ya_E(f_M(V0,f_a_E(T_ON,y0)),Sm,ro_ON,y0,V0,f_alfa(teta0,Teta_c0)),
//                                   f_m(m0,msek,t_0),V0,g,Teta_c0);
//        Kdx1       =   dt*f_dx_dt(V0,Teta_c0);
//        Kdy1       =   dt*f_dy_dt(V0,Teta_c0);
//        Kdw_z1     =   dt*f_domega_z_dt(f_M_z_E(f_M(V0,f_a_E(T_ON,y0)),Sm,ro_ON,y0,V0,ld),
//                                    J_z,
//                                    f_alfa(teta0,Teta_c0));
//        Kdteta1    =   dt*f_dteta(w_z0);
//    //пересчет переменных------------------------------------------------------------------------
//        V1      = V0      +KdV1*0.5;
//        Teta_c1 = Teta_c0 +KdTeta_c1*0.5;
//        x1      = x0      +Kdx1*0.5;
//        y1      = y0      +Kdy1*0.5;
//        w_z1    = w_z0    +Kdw_z1*0.5;
//        teta1   = teta0   +Kdteta1*0.5;
//        t1      = t_0     +dt*0.5;

//    //K2____________________________________________________________________________
//        KdV2       =   dt*f_dV_dt(f_P_E(f_P0(msek),Sa,p_ON,y1),
//                      f_alfa(teta1,Teta_c1),
//                      f_Xa_E(f_M(V1,f_a_E(T_ON,y1)),Sm,ro_ON,y1,V1),
//                      f_m(m0,msek,t1),
//                      g,
//                      Teta_c1);
//        KdTeta_c2  =   dt*f_dTeta_c_dt(f_P_E(f_P0(msek),Sa,p_ON,y1),
//                                   f_alfa(teta1,Teta_c1),
//                                   f_Ya_E(f_M(V1,f_a_E(T_ON,y1)),Sm,ro_ON,y1,V1,f_alfa(teta1,Teta_c1)),
//                                   f_m(m0,msek,t1),V1,g,Teta_c1);
//        Kdx2       =   dt*f_dx_dt(V1,Teta_c1);
//        Kdy2       =   dt*f_dy_dt(V1,Teta_c1);
//        Kdw_z2     =   dt*f_domega_z_dt(f_M_z_E(f_M(V1,f_a_E(T_ON,y1)),Sm,ro_ON,y1,V1,ld),
//                                    J_z,
//                                    f_alfa(teta1,Teta_c1));
//        Kdteta2    =   dt*f_dteta(w_z1);
//    //пересчет переменных------------------------------------------------------------------------
//        V2      = V0      +KdV2*0.5;
//        Teta_c2 = Teta_c0 +KdTeta_c2*0.5;
//        x2      = x0      +Kdx2*0.5;
//        y2      = y0      +Kdy2*0.5;
//        w_z2    = w_z0    +Kdw_z2*0.5;
//        teta2   = teta0   +Kdteta2*0.5;
//        t2      = t_0     +dt*0.5     ;

//    //K3____________________________________________________________________________
//        KdV3       =   dt*f_dV_dt(f_P_E(f_P0(msek),Sa,p_ON,y2),
//                              f_alfa(teta2,Teta_c2),
//                              f_Xa_E(f_M(V2,f_a_E(T_ON,y2)),Sm,ro_ON,y2,V2),
//                              f_m(m0,msek,t2),
//                              g,
//                              Teta_c2);
//        KdTeta_c3  =   dt*f_dTeta_c_dt(f_P_E(f_P0(msek),Sa,p_ON,y2),
//                                   f_alfa(teta2,Teta_c2),
//                                   f_Ya_E(f_M(V2,f_a_E(T_ON,y2)),Sm,ro_ON,y2,V2,f_alfa(teta2,Teta_c2)),
//                                   f_m(m0,msek,t2),V2,g,Teta_c2);
//        Kdx3       =   dt*f_dx_dt(V2,Teta_c2);
//        Kdy3       =   dt*f_dy_dt(V2,Teta_c2);
//        Kdw_z3     =   dt*f_domega_z_dt(f_M_z_E(f_M(V2,f_a_E(T_ON,y2)),Sm,ro_ON,y2,V2,ld),
//                                    J_z,
//                                    f_alfa(teta2,Teta_c2));
//        Kdteta3    =   dt*f_dteta(w_z2);
//    //пересчет переменных------------------------------------------------------------------------
//        V3      = V0      +KdV3;
//        Teta_c3 = Teta_c0 +KdTeta_c3;
//        x3      = x0      +Kdx3;
//        y3      = y0      +Kdy3;
//        w_z3    = w_z0    +Kdw_z3;
//        teta3   = teta0   +Kdteta3;
//        t3      = t_0     +dt     ;

//    //K4___________________________________________________________________________
//        KdV4       =   dt*f_dV_dt(f_P_E(f_P0(msek),Sa,p_ON,y3),
//                              f_alfa(teta3,Teta_c3),
//                              f_Xa_E(f_M(V3,f_a_E(T_ON,y3)),Sm,ro_ON,y3,V3),
//                              f_m(m0,msek,t3),
//                              g,
//                              Teta_c3);
//        KdTeta_c4  =   dt*f_dTeta_c_dt(f_P_E(f_P0(msek),Sa,p_ON,y3),
//                                   f_alfa(teta3,Teta_c3),
//                                   f_Ya_E(f_M(V3,f_a_E(T_ON,y3)),Sm,ro_ON,y3,V3,f_alfa(teta3,Teta_c3)),
//                                   f_m(m0,msek,t3),V3,g,Teta_c3);
//        Kdx4       =   dt*f_dx_dt(V3,Teta_c3);
//        Kdy4       =   dt*f_dy_dt(V3,Teta_c3);
//        Kdw_z4     =   dt*f_domega_z_dt(f_M_z_E(f_M(V3,f_a_E(T_ON,y3)),Sm,ro_ON,y3,V3,ld),
//                                    J_z,
//                                    f_alfa(teta3,Teta_c3));
//        Kdteta4    =   dt*f_dteta(w_z3);

//    //Расчет шага__________________________________________________________________
//        V0      += (KdV1+2*KdV2+2*KdV3+KdV4)/6.0;
//        Teta_c0 += (KdTeta_c1+2*KdTeta_c2+2*KdTeta_c3+KdTeta_c4)/6.0  ;
//        x0      += (Kdx1+2*Kdx2+2*Kdx3+Kdx4)/6.0                      ;
//        y0      += (Kdy1+2*Kdy2+2*Kdy3+Kdy4)/6.0                      ;
//        w_z0    += (Kdw_z1+2*Kdw_z2+2*Kdw_z3+Kdw_z4)/6.0              ;
//        teta0   += (Kdteta1+2*Kdteta2+2*Kdteta3+Kdteta4)/6.0          ;
//        t_0     += dt                                                      ;


//     }

    return 0.0;
}

double Optima(double a2, double a1, double a0, tExport *data)
{
    int i=1;
    double x_cur = 100.0, x_pr = 0.0, da1=2E-4, da2=1E-5, step1 = 2*da1, step2 = 2*da2;
    QVdouble a_cur = {a0, a1, a2};
    QVdouble a_0 = {a0, a1, a2};
    QVdouble grad_a = {0.0, 0.0};
    QVdouble x_cur_ARR = {0.0, 0.0, 0.0};
    while (fabs(x_cur-x_pr) > 1E-6)
    {

        x_pr = x_cur;

        x_cur_ARR[0] = IntegratorDP_E1(a_cur[2], a_cur[1], a_cur[0]);
        x_cur_ARR[1] = IntegratorDP_E1(a_cur[2], a_cur[1]+da1, a_cur[0]);
        x_cur_ARR[2] = IntegratorDP_E1(a_cur[2]+da2, a_cur[1], a_cur[0]);

        grad_a[0]=(x_cur_ARR[1]-x_cur_ARR[0])/da1;
        grad_a[1]=(x_cur_ARR[2]-x_cur_ARR[0])/da2;

        a_cur[1]+=copysign(step1,grad_a[0]);
        a_cur[2]+=copysign(step2,grad_a[1]);

        x_cur=x_cur_ARR[0];

       i++;
       if (i%10==0) {
           step1/=2;
           step2/=2;
       }
    }
    IntegratorDP_E1(a_cur[2], a_cur[1], a_cur[0], data);
    return x_cur;
}

double Optima3(double a2, double a1, double a0, tExport *data)
{
    int i=1;
    double x_cur = 100.0, x_pr = 0.0, da0=1E-4, da1=2E-4, da2=1E-5, step0 = 2*da0, step1 = 2*da1, step2 = 2*da2;
    QVdouble a_cur = {a0, a1, a2};
    QVdouble a_0 = {a0, a1, a2};
    QVdouble grad_a = {0.0, 0.0, 0.0};
    QVdouble x_cur_ARR = {0.0, 0.0, 0.0};
    while (fabs(x_cur-x_pr) > 1E-6)
    {

        x_pr = x_cur;

        x_cur = IntegratorDP_E1(a_cur[2], a_cur[1], a_cur[0]);
        x_cur_ARR[0] = IntegratorDP_E1(a_cur[2], a_cur[1], a_cur[0] + da0);
        x_cur_ARR[1] = IntegratorDP_E1(a_cur[2], a_cur[1]+da1, a_cur[0]);
        x_cur_ARR[2] = IntegratorDP_E1(a_cur[2]+da2, a_cur[1], a_cur[0]);

        grad_a[0]=(x_cur_ARR[0]-x_cur)/da0;
        grad_a[1]=(x_cur_ARR[1]-x_cur)/da1;
        grad_a[2]=(x_cur_ARR[2]-x_cur)/da2;


        a_cur[0]+=copysign(step0,grad_a[0]);
        a_cur[1]+=copysign(step1,grad_a[1]);
        a_cur[2]+=copysign(step2,grad_a[2]);

       i++;
       if (i%10==0) {
           step0/=2;
           step1/=2;
           step2/=2;
       }
    }
    IntegratorDP_E1(a_cur[2], a_cur[1], a_cur[0], data);
    return x_cur;
}

double Optima1(double a2, double a1, double a0, tExport *data)
{
    int i=1;
    double x_cur = 100.0, x_pr = 0.0, da0=1E-3, da1=2E-4, da2=1E-5, step0 = 2*da0, step1 = 2*da1, step2 = 2*da2;
    QVdouble a_cur = {a0, a1, a2};
    QVdouble a_0 = {a0, a1, a2};
    QVdouble grad_a = {0.0, 0.0, 0.0};
    QVdouble x_cur_ARR = {0.0, 0.0, 0.0};
    while (fabs(x_cur-x_pr) > 1E-6)
    {

        x_pr = x_cur;

        x_cur = IntegratorDP_E1(a_cur[2], a_cur[1], a_cur[0]);
        x_cur_ARR[0] = IntegratorDP_E1(a_cur[2], a_cur[1], a_cur[0] + da0);

        grad_a[0]=(x_cur_ARR[0]-x_cur)/da0;

        a_cur[0]+=copysign(step0,grad_a[0]);

       i++;
       if (i%10==0) {
           step0/=2;

       }
    }
    IntegratorDP_E1(a_cur[2], a_cur[1], a_cur[0], data);
    return x_cur;
}

QVVdouble Optima_s(double pa2_0, double pa1_0, double pa2_k, double pa1_k, double step_a2,double step_a1)
{
    QVVdouble out;//= QVVdouble(S_a2).fill(QVdouble(S_a1).fill(0.0));
    double  a_1 = pa1_0;
    QVdouble row1;
    while (a_1 <= pa1_k) {
        row1.append(a_1);
        a_1 += step_a1;
    }
    out.append(row1);


    double  a2 = pa2_0;
    while (a2 >= pa2_k) {
        QVdouble row;
        double  a1 = pa1_0;
        row.append(a2);
        while (a1 <= pa1_k) {
            row.append(IntegratorDP_E1(a2, a1, 0));
            a1 += step_a1;
        }
        out.append(row);
        a2 += step_a2;
    }

    return out;
}

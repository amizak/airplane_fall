#ifndef GOST4401_81_H
#define GOST4401_81_H
#include "additional_func.h"

typedef struct {
    double p,ro,a,T,mu,nu;
}atm_param;

class gost_4401_21
{
    const int N = 10;
    QVdouble H_b = { 0, -2000, 0, 11000, 20000, 32000, 47000, 51000, 71000, 85000 };
    QVdouble h = { 0, -1.99937E3, 0, 11.01907E3, 20.06312E3, 32.1619E3, 47.35009E3, 51.41248E3, 71.80197E3, 86.15199E3 };
    QVdouble T = { 0, 301.15, 288.15, 216.65, 216.65, 228.65, 270.65, 270.65, 214.65, 186.65 };
    QVdouble beta = { 0, -6.5E-3, -6.5E-3, 0, 1E-3, 2.8E-3, 0, -2.8E-3, -2E-3, 0 };

    const double p0 = 101325,       /// давление
                 T0 = 288.15,       /// температура Кельвина
                 gs = 9.80665,      /// ускорение свободного падения
                  X = 1.4,          /// показатель адиабаты
                  R = 287.05287,    /// удельная газовая постояннная
                  S = 110.4,
                 Bs = 1.458E-6,
                Rzs = 6356767;      /// радиус

    QVdouble p = QVdouble(N).fill(0.0),
            ro = QVdouble(N).fill(0.0),
             a = QVdouble(N).fill(0.0),
            mu = QVdouble(N).fill(0.0),
            nu = QVdouble(N).fill(0.0);

    double pv, rov, av, muv, nuv, Tv, H_g;

    double f_pb0(double H, double Hn, double T1, double pn);

    double f_pb(double H, double Hn, double T1z, double pn, double beta);

    double f_ro(double p, double T1);

    double f_a(double T1);

    double f_mu(double T1);

    double f_nu(double mu, double roo);


public:
    gost_4401_21();

    atm_param f_gost_param (double y, bool t_v = true);
// t_v=true -- геометрическа высота|false -- геопотенциальная высота;
    double p_gost  (double y, bool t_v = true);
    double ro_gost (double y, bool t_v = true);
    double a_gost  (double y, bool t_v = true);
    double T_gost  (double y, bool t_v = true);
    double h_gost  (double H_g);
    double H_g_gost(double h);

};

#endif // GOST4401_81_H

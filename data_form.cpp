#include "data_form.h"


TableModelFullparams::TableModelFullparams(QObject *parent) : QAbstractTableModel(parent)
{
    for (int i = 0; i < vExport::ev_LAST; ++i) {
        visible_order[i] = i;
        show_order[i] = i;
    }
}

TableModelFullparams::TableModelFullparams(tExport *table_data, QObject *parent)
{
    for (int i = 0; i < vExport::ev_LAST; ++i) {
        visible_order[i] = i;
        show_order[i] = i;
    }
    addTable(table_data);
}

//TableModelParams::TableModelParams(const TableModelParams &model, QObject *parent) : QAbstractTableModel(parent)
//{

//}

int TableModelFullparams::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED( parent )
//    return table.count();
    return ptr_data_table->getRowCount();
}

int TableModelFullparams::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED( parent )
//    return Column::p_LAST;
    int colcount = visible_order.count();
//    for (int i = 0; i < data_table.Settings.visible.count(); ++i) {
//        if (data_table.Settings.visible[i]) {
//            colcount += 1;
//        }
//    }
    return colcount;
}

QVariant TableModelFullparams::data(const QModelIndex &index, int role) const
{
//    if(
//        !index.isValid() ||
//        table.count() <= index.row() ||
//        ( role != Qt::DisplayRole && role != Qt::EditRole )
//    ) {
//        return QVariant();
//    }

    if(
        !index.isValid() ||
//        table.count() <= index.row()
        ptr_data_table->getRowCount() <= index.row()
    ) {
        return QVariant();
    }

    int column_num = visible_order[index.column()];
    switch (role) {
    case Qt::DisplayRole:{
//        if (data_table.Settings.visible[index.column()]) {
        if (column_num < ptr_data_table->getColumnCount()){
            return QString::number(ptr_data_table->getVal(column_num,index.row()),
                                   ptr_data_table->Settings.format[column_num].toLatin1(),
                                   ptr_data_table->Settings.kolzn[column_num]);
        } else {
            return QVariant();
        }
    }
//    }
    case Qt::SizeHintRole:{
        int width;

            width = ptr_data_table->Settings.kolzn[column_num]*6+25;


        return QSize(width,20);
    }
    default:{
        return  QVariant();
    }
    }

}



QVariant TableModelFullparams::headerData(int section, Qt::Orientation orientation, int role) const
{
    if( role != Qt::DisplayRole ) {
        return QVariant();
    }

    if( orientation == Qt::Vertical ) {
        return QVariant();                  //номера строк
    }

//    return headerName[section];
    return ptr_data_table->SLname[visible_order[section]];


//    return QVariant();
}

Qt::ItemFlags TableModelFullparams::flags(const QModelIndex &index) const
{
//    Qt::ItemFlags flags = QAbstractTableModel::flags( index );
    if( index.isValid() ) {
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
//        return Qt::NoItemFlags;
    }
    return Qt::NoItemFlags;
//    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

void TableModelFullparams::addTable(tExport *table_data)
{
    clearALL();
    ptr_data_table = table_data;
    beginInsertRows( QModelIndex(), 0, table_data->getRowCount()-1 );

    endInsertRows();

    int colcount = 0;
    for (int i = 0; i < ptr_data_table->Settings.visible.count(); ++i) {
        bool isVisiable = ptr_data_table->Settings.visible.at(i);
        if (isVisiable) {
            colcount += 1;
        }
    }
    show_order.fill(0,colcount);
    visible_order.fill(0,colcount);
    int j = 0;
    for (int i = 0; i < ptr_data_table->Settings.visible.count(); ++i) {
        if (ptr_data_table->Settings.visible[i]) {
            visible_order[j] = i;
            show_order[j] = ptr_data_table->Settings.shOrder.at(i);
            j++;
        }
    }


}

void TableModelFullparams::clearALL()
{
    if(ptr_data_table != nullptr){
        if (!ptr_data_table->data.isEmpty()){
            beginRemoveRows( QModelIndex(), 0, ptr_data_table->getRowCount() - 1 );
            ptr_data_table->data.clear();
            endRemoveRows();
        }
    }

}

////____________Data Table Settings______________________________________________////
TM_TableSettings::TM_TableSettings(QObject *parent){}

TM_TableSettings::~TM_TableSettings(){}

int TM_TableSettings::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED( parent )
    return ED_Settings::s_LAST;
}

int TM_TableSettings::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED( parent )
    return vExport::ev_LAST;
}

QVariant TM_TableSettings::data(const QModelIndex &index, int role) const
{
    if(
        !index.isValid() ||
        ED_Settings::s_LAST <= index.row()
    ) {
        return QVariant();
    }

    switch (role) {
    case Qt::EditRole:
    case Qt::DisplayRole:{
        switch (index.row()) {
        case ED_Settings::s_Num_Visibility:{
            return QVariant(data_table.Settings.visible.at(index.column()));
        }
        case ED_Settings::s_Num_showOrder:{
            return QVariant(data_table.Settings.shOrder.at(index.column()));
        }
        case ED_Settings::s_Num_format:{
            return QVariant(data_table.Settings.format.at(index.column()));
        }
        case ED_Settings::s_Num_floatOrder:{
            return QVariant(data_table.Settings.kolzn.at(index.column()));
        }
        case ED_Settings::s_Num_dimType:{
            return QVariant(data_table.Settings.dimType.at(index.column()));
        }
        case ED_Settings::s_Num_dimOrder:{
            return QVariant(data_table.Settings.dimOrder.at(index.column()));
        }
        default:
            return  QVariant();
        }
    }

    default:{
        return  QVariant();
    }
    }
}

bool TM_TableSettings::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if( !index.isValid() || role != Qt::EditRole || ED_Settings::s_LAST <= index.row() ) {
        return false;
    }
    if (Qt::EditRole == role){
    switch (index.row()) {
    case ED_Settings::s_Num_Visibility:{
        data_table.Settings.visible.replace(index.column(),value.toBool());
        emit dataChanged( index, index );
        return true;
    }
    case ED_Settings::s_Num_showOrder:{
        data_table.Settings.shOrder.replace(index.column(),value.toInt());
        emit dataChanged( index, index );
        return true;
    }
    case ED_Settings::s_Num_format:{
        data_table.Settings.format.replace(index.column(),value.toChar());
        emit dataChanged( index, index );
        return true;
    }
    case ED_Settings::s_Num_floatOrder:{
        data_table.Settings.kolzn.replace(index.column(),value.toInt());;
        emit dataChanged( index, index );
        return true;
    }
    case ED_Settings::s_Num_dimType:{
        data_table.Settings.dimType.replace(index.column(),value.toInt());;
        emit dataChanged( index, index );
        return true;
    }
    case ED_Settings::s_Num_dimOrder:{
        data_table.Settings.dimOrder.replace(index.column(),value.toInt());;
        emit dataChanged( index, index );
        return true;
    }
    default:
        return false;

    }
    }

    return false;
}

QVariant TM_TableSettings::headerData(int section, Qt::Orientation orientation, int role) const
{
    if( role != Qt::DisplayRole ) {
        return QVariant();
    }

    if( orientation == Qt::Vertical && ED_Settings::s_LAST > section ) {
        return data_table.Settings.Settings_name_List.at(section);//QString("a")+QString(section);                  //номера строк
    }

    if( orientation == Qt::Horizontal ) {
        return data_table.SLname[section];                  //номера строк
    }
//    return headerName[section];

    return QVariant();

}

Qt::ItemFlags TM_TableSettings::flags(const QModelIndex &index) const
{
    if(!index.isValid() ||
        ED_Settings::s_LAST <= index.row()
    ) {
        return Qt::NoItemFlags;
    }

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;

}

void TM_TableSettings::setDataTable(tExport &table_data)
{
    clearALL();
    beginInsertRows( QModelIndex(), 0, 5 );
    data_table = table_data;
    endInsertRows();
}

void TM_TableSettings::clearALL()
{
    beginRemoveRows( QModelIndex(), 0, 5 );
    endRemoveRows();

}

ED_Settings TM_TableSettings::getSettings()
{
    return data_table.Settings;
}

////_________________________________________________////

LM_Show_Tabletype::LM_Show_Tabletype(QObject *parent)
{
//    BD_Settings tempBDS;
    namelist = ED_Settings().Settings_preset_name_List;
}

int LM_Show_Tabletype::rowCount(const QModelIndex &parent) const
{
    return  namelist.length();
}

QVariant LM_Show_Tabletype::data(const QModelIndex &index, int role) const
{
    if(
        !index.isValid() ||
        ED_Settings::i_LAST <= index.row()
    ) {
        return QVariant();
    }

    switch (role) {
    case Qt::UserRole:
    case Qt::DisplayRole:{
        switch (index.row()) {
        case ED_Settings::i_All:{
            return namelist.at(index.row());
        }
        case ED_Settings::i_Custom:{
            return namelist.at(index.row());
        }
        default:
            return QVariant();
        }
    }

    default:{
        return  QVariant();
    }
    }
}

bool LM_Show_Tabletype::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if( !index.isValid() || role != Qt::EditRole || ED_Settings::s_LAST <= index.row() ) {
        return false;
    }
    if (Qt::EditRole == role){
    switch (index.row()) {
    case ED_Settings::i_All:{
        namelist.at(index.row());
        emit dataChanged( index, index );
        return true;
    }
    case ED_Settings::i_Custom:{
        namelist.at(index.row());
        emit dataChanged( index, index );
        return true;
    }
    default:
        return false;

    }
    }

    return false;
}

Qt::ItemFlags LM_Show_Tabletype::flags(const QModelIndex &index) const
{
    if(!index.isValid() ||
        ED_Settings::s_LAST <= index.row()
    ) {
        return Qt::NoItemFlags;
    }
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable;
}



LM_Plot_xAxes::LM_Plot_xAxes(QObject *parent)
{
    namelist = tExport().SLname;
}

int LM_Plot_xAxes::rowCount(const QModelIndex &parent) const
{
    return  namelist.length();
}

QVariant LM_Plot_xAxes::data(const QModelIndex &index, int role) const
{
    if(!index.isValid() ||
        vExport::ev_LAST <= index.row()) {
        return QVariant();
    }

    switch (role) {
    case Qt::UserRole:
    case Qt::DisplayRole:{
        if(index.row() < vExport::ev_LAST) {
            return namelist.at(index.row());
        }
    }

    default:{
        return  QVariant();
    }
    }
}

bool LM_Plot_xAxes::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if( !index.isValid() || role != Qt::EditRole || ED_Settings::s_LAST <= index.row() ) {
        return false;
    }
    if (Qt::EditRole == role){
        if(index.row() < vExport::ev_LAST) {
        namelist.at(index.row());
        emit dataChanged( index, index );
        return true;
    }
    }

    return false;
}

Qt::ItemFlags LM_Plot_xAxes::flags(const QModelIndex &index) const
{
    if(!index.isValid() ||
        vExport::ev_LAST <= index.row()
    ) {
        return Qt::NoItemFlags;
    }
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable;

}

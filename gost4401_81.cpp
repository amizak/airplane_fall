#include "gost4401_81.h"
#include <iostream>
#include "math.h"

using namespace std;


gost_4401_21::gost_4401_21()
{
    //________________________Рассчет в реперных точках_______________________

        p[2] = p0;
        p[1] = f_pb(H_b[1], H_b[2], T[2], p[2], beta[1]);
        ro[1] = f_ro(p[1], T[1]);
        a[1] = f_a(T[1]);
        mu[1] = f_mu(T[1]);
        nu[1] = f_nu(mu[1], ro[1]);

        for (int i = 2; i < N-1; i++) {
            if (beta[i] == 0) {
                p[i + 1] = f_pb0(H_b[i + 1], H_b[i], T[i + 1], p[i]);
            }
            else  p[i + 1] = f_pb(H_b[i + 1], H_b[i], T[i], p[i], beta[i]);
            ro[i] = f_ro(p[i], T[i]);
            a[i] = f_a(T[i]);
            mu[i] = f_mu(T[i]);
            nu[i] = f_nu(mu[i], ro[i]);
        }

}

double gost_4401_21::f_pb0(double H, double Hn, double T1, double pn) {
    return pn * exp(-gs / (R*T1)*(H - Hn));
}

double gost_4401_21::f_pb(double H, double Hn, double T1z, double pn, double beta) {
    return pn * pow(1 + beta*(H - Hn) / T1z, -gs / (R*beta));
}

double gost_4401_21::f_ro(double p, double T1) {
    return p / (R*T1);
}

double gost_4401_21::f_a(double T1) {
    return pow(X*R*T1, 0.5);
}

double gost_4401_21::f_mu(double T1) {
    return Bs*pow(T1, 1.5) / (T1 + S);
}

double gost_4401_21::f_nu(double mu, double roo) {
    return mu / roo;
}



double gost_4401_21::H_g_gost(double h) {
    return Rzs*h / (Rzs + h);
}

double gost_4401_21::h_gost(double H_g) {
    return Rzs*H_g / (Rzs - H_g);
}

// t_v=true -- геометрическа высота; t_v=false -- геопотенциальная высота;
double gost_4401_21::p_gost(double y, bool t_v) {


//________________________Рассчет конкретной высоты_______________________

// t_v=true -- геометрическа высота|false -- геопотенциальная высота;

    if (t_v) {
            H_g = H_g_gost(y); // y -- геометрическая высота
    }
    else {
            H_g = y;        // y -- геопотенциальная высота
    }

    int j = 0;

    for (int i = 1; i < N-1; i++) {
        if ((H_b[i] <= H_g) && (H_g < H_b[i + 1])) {
            Tv = T[i] + beta[i] * (H_g - H_b[i]);
            j = i;
        }
    }

    if (beta[j] == 0) {
        pv = f_pb0(H_g, H_b[j], Tv, p[j]);
    } else {
        pv = f_pb(H_g, H_b[j], T[j], p[j], beta[j]);
    }
    if ((y==-2000)&&(t_v)) {
        pv = f_pb(H_g_gost(H_b[1]), H_b[2], T[2], p[2], beta[1]);
    }
  /*rov = f_ro(pv, Tv);
    av = f_a(Tv);
    muv = f_mu(Tv);
    nuv = f_nu(muv, rov);
  */
    return pv;
}

double gost_4401_21::ro_gost(double y, bool t_v) {

    //________________________Рассчет конкретной высоты_______________________

    // t_v=true -- геометрическа высота; t_v=false -- геопотенциальная высота;

        if (t_v) {
                H_g = H_g_gost(y); // y -- геометрическая высота
        }
        else {
                H_g = y;        // y -- геопотенциальная высота
        }

        int j = 0;

        for (int i = 1; i < N-1; i++) {
            if ((H_b[i] <= H_g) && (H_g < H_b[i + 1])) {
                Tv = T[i] + beta[i] * (H_g - H_b[i]);
                j = i;
            }
        }
        if (beta[j] == 0) {
            pv = f_pb0(H_g, H_b[j], Tv, p[j]);
        } else {
            pv = f_pb(H_g, H_b[j], T[j], p[j], beta[j]);
        }
        if ((y==-2000)&&(t_v)) {
            pv = f_pb(H_g_gost(H_b[1]), H_b[2], T[2], p[2], beta[1]);
        }
        rov = f_ro(pv, Tv);
    /*
        av = f_a(Tv);
        muv = f_mu(Tv);
        nuv = f_nu(muv, rov);
    */
    return rov;

}

double gost_4401_21::a_gost(double y, bool t_v) {

    //________________________Рассчет конкретной высоты_______________________

    // t_v=true -- геометрическа высота; t_v=false -- геопотенциальная высота;

        if (t_v) {
                H_g = H_g_gost(y); // y -- геометрическая высота
        }
        else {
                H_g = y;        // y -- геопотенциальная высота
        }

        int j = 0;

        for (int i = 1; i < N-1; i++) {
            if ((H_b[i] <= H_g) && (H_g < H_b[i + 1])) {
                Tv = T[i] + beta[i] * (H_g - H_b[i]);
                j = i;
            }
        }

        if (beta[j] == 0) {
            pv = f_pb0(H_g, H_b[j], Tv, p[j]);
        } else {
            pv = f_pb(H_g, H_b[j], T[j], p[j], beta[j]);
        }
        if ((y==-2000)&&(t_v)) {
            pv = f_pb(H_g_gost(H_b[1]), H_b[2], T[2], p[2], beta[1]);
        }


        rov = f_ro(pv, Tv);
        av = f_a(Tv);
        //muv = f_mu(Tv);
        //nuv = f_nu(muv, rov);

    return av;

}

double gost_4401_21::T_gost(double y, bool t_v) {

    //________________________Рассчет конкретной высоты_______________________

    // t_v=true -- геометрическа высота; t_v=false -- геопотенциальная высота;

        if (t_v) {
                H_g = H_g_gost(y); // y -- геометрическая высота
        }
        else {
                H_g = y;        // y -- геопотенциальная высота
        }



        for (int i = 1; i < N-1; i++) {
            if ((H_b[i] <= H_g) && (H_g <= H_b[i + 1])) {
                Tv = T[i] + beta[i] * (H_g - H_b[i]);

            }
        }
    return Tv;
}

atm_param gost_4401_21::f_gost_param (double y, bool t_v) {

    atm_param gost_param;

    //________________________Рассчет конкретной высоты_______________________

    // t_v=true -- геометрическа высота; t_v=false -- геопотенциальная высота;

        if (t_v) {
                H_g = H_g_gost(y); // y -- геометрическая высота
        }
        else {
                H_g = y;        // y -- геопотенциальная высота
        }

        int j = 0;

        for (int i = 1; i < N-1; i++) {
            if ((H_b[i] <= H_g) && (H_g <= H_b[i + 1])) {
                gost_param.T = T[i] + beta[i] * (H_g - H_b[i]);
                j = i;
            }
        }

                if (beta[j] == 0) {
                    gost_param.p = f_pb0(H_g, H_b[j], gost_param.T, p[j]);
                } else {
                    gost_param.p = f_pb(H_g, H_b[j], T[j], p[j], beta[j]);
                }
                if ((y==-2000)&&(t_v)) {
                    gost_param.p = f_pb(H_g_gost(H_b[1]), H_b[2], T[2], p[2], beta[1]);
                }
        gost_param.ro = f_ro(gost_param.p, gost_param.T);
        gost_param.a = f_a(gost_param.T);
        gost_param.mu = f_mu(gost_param.T);
        gost_param.nu = f_nu(gost_param.mu, gost_param.ro);

        return gost_param;
}


#include "mathmodel.h"

using namespace std;

const int num=15;
const double Pi0 = 398600.44;
const double Rearth = 6371000;
const double g_Earth = 9.81;

double Mah[num] ={0.01, 0.55, 0.8, 0.9, 1.0, 1.06, 1.1, 1.2, 1.3, 1.4, 2.0, 2.6, 3.4, 6.0, 10.0};
double Cxa[num] ={0.3, 0.3, 0.55, 0.7, 0.84, 0.86, 0.87, 0.83, 0.8, 0.79, 0.65, 0.55, 0.50, 0.45, 0.40};
double Cya[num] ={0.25, 0.25, 0.25, 0.20, 0.30, 0.31, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25};

double f_dV_dt(double Xa, double m, double Teta_c){
    return (Xa)/m-g_Earth*sin(Teta_c);
}

double f_dTeta_c_dt(double Ya, double m, double V, double Teta_c){
    if (V != 0.0){
        return (Ya)/m/V-g_Earth*cos(Teta_c)/V;
    } else {
        return 0.0;
    }
}

double f_dx_dt(double V, double Teta_c){
    return V*cos(Teta_c);
}

double f_dy_dt(double V, double Teta_c){
    return V*sin(Teta_c);
}
double f_domega_z_dt(double M_z, double J_z){
    return M_z/J_z;
}
double f_dteta(double omega_z, double om_a){
    return omega_z + om_a;
}
double f_alfa(double teta,double Teta_c){
    return teta-Teta_c;
}
double f_alpha(double a0,double a1,double a2,double t){
    return a0+a1*t+a2*t*t;
}

//==== Сила лобового сопротивления в разных формах=========================
double f_Xa_E(double alpha,double Sm,double ro,double V){
    return -f_C_xa(fabs(alpha*toD))*Sm*(ro/2)*pow(V,2);
}

double f_Xa_gost(double alpha,double Sm,double y,double V){
    gost_4401_21 atm;
    return f_C_xa(fabs(alpha))*Sm*(atm.ro_gost(y,1)/2)*pow(V,2);
}

double f_Xa(double Cxa,double Sm,double ro,double V){
    return Cxa*Sm*(ro/2)*pow(V,2);
}

//==== Подъемная сила в разных формах=========================
double f_Ya_E(double alpha, double Sm,double ro,double V){
    return f_C_ya(alpha*toD)*Sm*(ro/2)*pow(V,2)*alpha;//*copysign(1,alpha);
}

double f_Ya_gost(double alpha,double Sm,double y,double V){
    gost_4401_21 atm;
    return f_C_ya(alpha)*Sm*(atm.ro_gost(y,1)/2)*pow(V,2)*alpha;
}

double f_Ya(double Cya,double Sm,double ro,double V,double alpha){
    return Cya*Sm*(ro/2)*pow(V,2)*alpha;
}

//==== Статический аэродинамический момент в разных формах=========================
double f_M_z_E(double alpha,double Sm,double ro,double y,double V,double l){
    return (f_mz(fabs(alpha*toD)))*Sm*(ro/2)*pow(V,2)*l*copysign(1,-alpha);
//    return -(f_C_xa(alpha)+f_C_ya(alpha))*Sm*(ro/2)*f_H(y)*pow(V,2)*l;
}

double f_M_z(double Cxa, double Cya,double Sm,double ro,double V,double l){
    return -(Cxa+Cya)*Sm*(ro/2)*pow(V,2)*l;
}


double f_M(double V,double a){
    return V/a;
}

double f_C_xa(double alpha){

     return cXa(alpha);
}

double f_C_ya(double alpha){

     return cYa(alpha);
}

double f_mz(double alpha){

     return mZomegaZ(alpha);
}

// Ускорение свободного падения
double f_g_p(double y){
    return Pi0 * pow(10, 9) / pow((Rearth + y), 2);
}

double cXa(double& alphaDegrees){

    static const double a = 1.184833992068E-3;
    static const double b = 9.730914798089E-1;
    static const double c = 2.075530417094;

    if (alphaDegrees <= 50)	{
        const double cXa = a * pow(b, alphaDegrees) * pow(alphaDegrees, c);
        return cXa;
    }else if (alphaDegrees > 50) {
//        throw("# error: alphaDegrees > 50");
    }
    return 0.0;
}

double cYa(double& alphaDegrees) {

    static const double a = 4.380843329689E-1;
    static const double b = 9.749281137226E-2;
    static const double c =-8.265531931536E-3;
    static const double d = 3.171970891046E-3;

    if (alphaDegrees < 0){
        const double cYa = (a + b * alphaDegrees) / (1 + c * alphaDegrees + d * alphaDegrees * alphaDegrees);
//        if (cYa < 0.0){
//            return 0.0;
//        }
        return cYa;
    }

    if (alphaDegrees < 35 && alphaDegrees >= 0){
        const double cYa = (a + b * alphaDegrees) / (1 + c * alphaDegrees + d * alphaDegrees * alphaDegrees);
        return cYa;
    }
    else if (alphaDegrees <= 50) {
        const double cYa = 0.825;
        return cYa;
    }else if (alphaDegrees > 50) {
//        throw("# error: alphaDegrees > 50");
    }

    return 0.0;
}

double mZomegaZ(double& alphaDegrees){

    const double alphaDegrees_ = abs(alphaDegrees);

    if (alphaDegrees >= 0 && alphaDegrees <= 10){
        static const double a = -4.14396192073169E-2;
        static const double b = 5.71896483678125E-3;
        static const double c = -6.5676673139262E-2;
        static const double d = 9.48318335088E-4;

        double mZomegaZ = (a + b * alphaDegrees) / (1 + c * alphaDegrees + d * alphaDegrees * alphaDegrees);
        return mZomegaZ;

    }else if (alphaDegrees > 10 && alphaDegrees < 50) {
        static const double a = 8.819044253344E-2;
        static const double b = -5.1187136521950E-3;
        static const double c = -5.7201921977231E-2;
        static const double d = 1.6649350954269E-3;

        double mZomegaZ = (a + b * alphaDegrees) / (1 + c * alphaDegrees + d * alphaDegrees * alphaDegrees);
        return mZomegaZ;

    }else if (alphaDegrees > 50) {
//        throw("# error: alphaDegrees > 50");
    }

    double mZomegaZ = NULL;
    return mZomegaZ;
}

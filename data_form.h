#ifndef DATA_FORM_H
#define DATA_FORM_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include "data_holder.h"

#include <QAbstractTableModel>

class TableModelFullparams: public QAbstractTableModel
{
    Q_OBJECT // Важно!!!
public:
    explicit TableModelFullparams (QObject *parent = nullptr);
    explicit TableModelFullparams (tExport *table_data,
                                   QObject *parent = nullptr);
//    explicit TableModelParams (const TableModelParams & model, QObject *parent = nullptr);
    virtual ~TableModelFullparams(){
        if (ptr_data_table != nullptr){
            delete ptr_data_table;
        }
    }

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    void addTable (tExport *table_data);
    void clearALL ();

private:

    QVector<int> visible_order  = QVector<int>(vExport::ev_LAST).fill(0);
    QVector<int> show_order     = QVector<int>(vExport::ev_LAST).fill(0);
    tExport* ptr_data_table = nullptr;

};

class TM_TableSettings: public QAbstractTableModel
{
    Q_OBJECT // Важно!!!
public:
    explicit TM_TableSettings (QObject *parent = nullptr);
    virtual ~TM_TableSettings();

public:
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    void setDataTable (tExport &table_data);
    void clearALL ();

    ED_Settings getSettings();
private:

    tExport data_table;

};

class LM_Show_Tabletype: public QAbstractListModel
{
    Q_OBJECT // Важно!!!
private:

    QStringList namelist;

public:
    explicit LM_Show_Tabletype (QObject *parent = nullptr);
    virtual ~LM_Show_Tabletype(){}

    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;

};

class LM_Plot_xAxes: public QAbstractListModel
{
    Q_OBJECT // Важно!!!
private:

    QStringList namelist;

public:
    explicit LM_Plot_xAxes (QObject *parent = nullptr);
    virtual ~LM_Plot_xAxes(){}

    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;

};

#endif // DATA_FORM_H

#include "data_holder.h"
#include "math.h"
#include "QDebug"



tExport::tExport()
{
//    this->data = new parametr_list();
}



void tExport::export_data_to_File(QString razd ,QString Path, QString FileName, QString Filetype, double every_time)
{
    QFile file_to_export(Path+FileName+Filetype);
    int max_length[vExport::ev_LAST] = {0};
//        parametr_list temp_data = data;
    bool rezhim_csv = false;
    if (Filetype == ".csv"){
        rezhim_csv = true;
    }

    int lev_otst, prav_otst = 0;

    for (int i = 0; i < vExport::ev_LAST; i++) {
        max_length[i] = SLname[i].length();
    }
    for (int i = 0; i < this->data.length(); i++) {
        for (int j = 0; j < vExport::ev_LAST; j++) {
            if (Settings.visible[j]){

                        int len = QString::number(this->data.at(i).at(j),
                                                  Settings.format[j].toLatin1(),
                                                  Settings.kolzn[j]).length();
                        if (len > max_length[j]){
                            max_length[j] = len;
                        }
            }
        }
    }

    double next_time_export = 0;
    bool printAll = false;
    if (every_time == 0.0){
        printAll = true;
    }

    QString export_string = "";
    if (file_to_export.open(QIODevice::WriteOnly | QIODevice::Text)){
        QTextStream exportStream(&file_to_export);
        exportStream.setLocale(QLocale(QLocale::Russian, QLocale::Russia));
        exportStream.setCodec("UTF-8");
        int raznica = 0;
        for (int i = 0; i < vExport::ev_LAST; i++) {
            if (Settings.visible[i]){
            raznica = (max_length[i] - this->SLname[i].length());
            if (max_length[i] > this->SLname[i].length()){
                prav_otst = raznica / 2;
                lev_otst = raznica - prav_otst;
            } else {
                prav_otst = 0;
                lev_otst = 0;
            }
            if (rezhim_csv) {
                prav_otst = 0;
                lev_otst = 0;
            }
            export_string += prob(lev_otst) + this->SLname[i] + prob(prav_otst) + razd;

        }
        }
        exportStream << export_string << endl;
        export_string = "";
        for (int i = 0; i < this->data.length(); i++) {
            if ((this->data.at(i).at(0) >= next_time_export) || (printAll) || i == this->data.length()) {
                next_time_export += every_time;
                for (int j = 0; j < vExport::ev_LAST; j++) {
                    if(Settings.visible[j]){
                                raznica = max_length[j] - QString::number(this->data.at(i).at(j),
                                                                          Settings.format[j].toLatin1(),
                                                                          Settings.kolzn[j]).length();
                                if ((raznica > 0) && (!rezhim_csv)){
                                    if (this->data.at(i).at(j) < 0.0){
                                        lev_otst = 0;
                                        prav_otst = raznica - lev_otst;
                                    } else {
                                        lev_otst = 1;
                                        prav_otst = raznica - lev_otst;
                                    }
                                } else {
                                    prav_otst = 0;
                                    lev_otst = 0;
                                }
                                export_string +=
                                        prob(lev_otst) +
                                        QString::number(this->data.at(i).at(j),
                                                        Settings.format[j].toLatin1(),
                                                        Settings.kolzn[j]) + prob(prav_otst) +
                                        razd;


                    }
                }
                exportStream << export_string << endl;
                export_string = "";
            }
        }
        file_to_export.close();
    }
// хотел сделать экспорт
}

vExport::vExport()
{
    data.reserve(ev_LAST);
    data.fill(0.0);
}

ED_Settings::ED_Settings(int init_type)
{
    set_preset_settings(init_type);
}

void ED_Settings::set_preset_settings(int type)
{
    switch (type) {
    case i_All:{
        for (int i = 0; i < vExport::ev_LAST; ++i) {
            shOrder.replace(i,i);
            switch (i) {
            case vExport::ev_Time_s:{
                visible.replace(i,true);
                format.replace(i,'f');
                kolzn.replace(i,6);
                dimType.replace(i,0);
                dimOrder.replace(i,0);
            } break;
            case vExport::ev_X_g    :
            case vExport::ev_Y_g    :
            case vExport::ev_Vx_g   :
            case vExport::ev_Vy_g   :
            case vExport::ev_om_z   :
            case vExport::ev_teta__r:
            case vExport::ev_teta__d:
            case vExport::ev_alpha_r:
            case vExport::ev_alpha_d:
            case vExport::ev_Teta_c_r:
            case vExport::ev_Teta_c_d:
            case vExport::ev_V      :
            case vExport::ev_r      :
//            case vExport::ev_Phi_r  :
//            case vExport::ev_Phi_d  :
//            case vExport::ev_Hi_r   :
//            case vExport::ev_Hi_d   :
            {
                visible.replace(i,true);
                format.replace(i,'f');
                kolzn.replace(i,9);
                dimType.replace(i,0);
                dimOrder.replace(i,0);
            } break;
            case vExport::ev_Density   :
            {
                visible.replace(i,true);
                format.replace(i,'e');
                kolzn.replace(i,6);
                dimType.replace(i,0);
                dimOrder.replace(i,0);
            } break;
            } break;
        }
    }break;
    default:{
        visible.fill(true);
        for (int i = 0; i < vExport::ev_LAST; ++i) {
            shOrder.replace(i,i);
            switch (i) {
            case vExport::ev_Time_s:{
                visible.replace(i,true);
                format.replace(i,'f');
                kolzn.replace(i,3);
                dimType.replace(i,0);
                dimOrder.replace(i,0);
            } break;
            case vExport::ev_X_g   :
            case vExport::ev_Y_g   :
            case vExport::ev_r      :
            case vExport::ev_Vx_g  :
            case vExport::ev_Vy_g  :
            case vExport::ev_V      :
            case vExport::ev_om_z  :
            case vExport::ev_teta__r:
            case vExport::ev_teta__d:
            case vExport::ev_Teta_c_r:
            case vExport::ev_Teta_c_d:
            case vExport::ev_alpha_r:
            case vExport::ev_alpha_d:
//            case vExport::ev_Phi_r  :
//            case vExport::ev_Phi_d  :
//            case vExport::ev_Hi_r   :
//            case vExport::ev_Hi_d   :
            {
                visible.replace(i,true);
                format.replace(i,'f');
                kolzn.replace(i,3);
                dimType.replace(i,0);
                dimOrder.replace(i,0);
            } break;
            case vExport::ev_Density   :
            {
                visible.replace(i,true);
                format.replace(i,'e');
                kolzn.replace(i,3);
                dimType.replace(i,0);
                dimOrder.replace(i,0);
            } break;
            } break;
        }
    }
    }

}
